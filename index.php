<!doctype html>
<html>
    <head>
        <title>mindmap</title>
        <script type="text/javascript" src="lib/json2.js"></script>
        <script type="text/javascript" src="lib/jquery-1.5.js"></script>
        <script type="text/javascript" src="lib/underscore-1.1.4.js"></script>
        <script type="text/javascript" src="lib/backbone-0.3.3.js"></script>

        <script type="text/javascript" src="lib/jquery.ui.core.js"></script>
        <script type="text/javascript" src="lib/jquery.ui.widget.js"></script>
        <script type="text/javascript" src="lib/jquery.ui.mouse.js"></script>
        <script type="text/javascript" src="lib/jquery.ui.position.js"></script>
        <script type="text/javascript" src="lib/jquery.ui.draggable.js"></script>
        <script type="text/javascript" src="lib/jquery.ui.droppable.js"></script>
        <script type="text/javascript" src="lib/script.js"></script>

        <script type="text/javascript" src="app/app.js"></script>

        <link rel="stylesheet" type="text/css" href="css/layout.css" />
        <link rel="stylesheet" type="text/css" href="css/jquery.ui.all.css" />
    </head>
    <body>
        <button id="new-map">new map</button>
		<div id="map"></div>
        <canvas id="canvas"></canvas>
    </body>
</html>
